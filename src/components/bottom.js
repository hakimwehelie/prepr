import { SocialIcon } from 'react-social-icons';
import styled from 'styled-components'
import logo from './cafeTerrrain.png'
import {Container} from 'react-bootstrap'
import {  Link as NavLink} from 'react-router-dom'

export default function Bottom(){

  const Bottom = styled.div
`
  display:flex;
  flex-shrink:0;
  width:100%;
  position:relative;
  bottom:0%;
  background-Color:#3C462D;
  border-top:4px solid #ebeae5;
@media (max-width: 990px) {
        height:10%;
        width: 100%;
        display:flex;
        flex-direction: column;
        position: relative;
        bottom:0px;
        z-index:-1;
   }
`;


  return(
      <Bottom>
          <div class="container" style={{zIndex:"100", width:"100%",position:"center",
          paddingBottom:"5px", minHight:"100vh", display: 'flex',  justifyContent:'center', alignItems:'center'}}
          className=" flex justify-items-center">
              <Container style={{justifyContent:"center", display:"grid"}}>


                      <div style={{display:"flex", position:"relative"}}>
                      <div style={{textAlign:"center",color:"#ebeae5"}}>
                      get to know us
                      <br/>
                      find your path
                      </div>
                      <img src={logo} width="70" height="70"/>
                      <div style={{textAlign:"center", color:"#ebeae5"}}>
                      say hello
                      </div>
                      </div>

              <div>

                </div>
                <div className="inline-flex py-1 px-1 my-1 mx-1 justify-items-center" style={{ position:"relative"
                ,justifyContent:"center"}}>
                      <SocialIcon
                        network="twitter"
                        target="_blank"

                        style={{height:30,width:30,marginRight:10}}
                        fgColor="#ebeae5"
                        bgColor="#3c462d"
                        />
                        <SocialIcon
                        network="facebook"
                        target="_blank"
                        fgColor="#ebeae5"
                        bgColor="#3c462d"
                        style={{height:30,width:30}}
                        />
                        <SocialIcon
                        network="instagram"
                        target="_blank"
                        fgColor="#ebeae5"
                        bgColor="#3c462d"
                        style={{height:30,width:30}}
                        />
                        <SocialIcon
                        network="pinterest"
                        target="_blank"
                        fgColor="#ebeae5"
                        bgColor="#3c462d"
                        style={{height:30,width:30}}
                        />

                  </div>
                  </Container>
            </div>
      </Bottom>
  )

}
