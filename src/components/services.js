import React, {  useEffect } from 'react';
import {Container} from "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
//import background from "./background.jpg"
import image from "./earlyBird.jpeg"
import brand from './media/brand1.png'


/*
#BFC8D1
#B6C1B4
#DDD7C3
#FB500B
#3C462D
*/


export default function Services(){

  useEffect(() => {
 document.title = "Services"
}, []);


return (


  <main className="relative" style={{justifyContent:"center", height:"100vh", backgroundColor:'#ebeae5' ,zIndex:-1}}>
<Container>

    <div id="cases" className="p-10 lg:pt-48 container mx-auto relative" style={{backgroundColor:"#ebeae5" }}>
        <section className=" rounded-lg  lg:flex p-20 my-10">
            <div className="text-lg lg:text-lg flex flex-col justify-center">
            <h1>Services</h1>

              <div>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                    Cum sociis natoque penatibus et magnis dis parturient montes,
                    nascetur ridiculus mus. Donec quam felis, ultricies nec,
                    pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                    Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                    In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
              </div>

            </div>
            <div style={{display:"contents"}}>
            <img src={brand} width='104px' height="142px"/>
            </div>
        </section>
        <section className=" rounded-lg  lg:flex p-20 my-10" style={{textAlign:"right"}}>

            <div className="text-lg lg:text-lg flex flex-col justify-right">
            <h1>Services</h1>
              <div>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                    Cum sociis natoque penatibus et magnis dis parturient montes,
                    nascetur ridiculus mus. Donec quam felis, ultricies nec,
                    pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                    Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                    In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
              </div>

            </div>
        </section>
    </div>

        </Container>
      </main>



);
}
