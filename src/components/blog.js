import React, { useEffect } from 'react';
import { Card,Row, Column ,Container} from "react-bootstrap"
import {Content} from './styles'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
//import background from "./background.jpg"
//import image from "./backgroundEdit.jpg"

// TODO: Add section for 'Our mission'
// TODO: Use a grid display to house fudged posts
// TODO: add comments about how wireframe images are used as placeholders

export default function Blog(){


  useEffect(() => {
 document.title = "Blog"
}, []);



return (


  <main className="relative" style={{justifyContent:"center", height:"100vh", backgroundColor:'#ebeae5' ,zIndex:-1}}>

  <Container>
          <div id="classes" className="p-10 lg:pt-48 container mx-auto relative" style={{display:'grid',justifyContent:"center",backgroundColor:"#ebeae5"}}>
          <Content style={{marginTop:"35px", display:"grid"}}>
                  <h1>Blog Posts</h1>
                    <Row xs={1} md={2} className="g-4" style={{marginLeft:"20px"}}>
                     <Card className="card-primary" style={{ width: '18rem', marginRight:"10px", marginBottom:"8px", backgroundColor:"" }}>
                              <Card.Body>
                                    <Card.Title>Blog</Card.Title>
                                      <Card.Subtitle className="mb-2 text-muted">Blog</Card.Subtitle>
                                          <Card.Text>
                                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                                          Cum sociis natoque penatibus
                                          </Card.Text>
                                          <Card.Link href="#">Card Link</Card.Link>
                                          <Card.Link href="#">Another Link</Card.Link>
                              </Card.Body>
                    </Card>
                    <Card className="card-primary" style={{ width: '18rem', marginRight:"10px", marginBottom:"8px" }}>
                    <Card.Body>
                          <Card.Title>Blog</Card.Title>
                            <Card.Subtitle className="mb-2 text-muted">Blog</Card.Subtitle>
                                <Card.Text>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                                Cum sociis natoque penatibus
                                </Card.Text>
                                <Card.Link href="#">Card Link</Card.Link>
                                <Card.Link href="#">Another Link</Card.Link>
                    </Card.Body>
                   </Card>

                   <Card className="card-primary" style={{ width: '18rem', marginRight:"10px", marginBottom:"8px" }}>
                   <Card.Body>
                         <Card.Title>Blog</Card.Title>
                           <Card.Subtitle className="mb-2 text-muted">Blog</Card.Subtitle>
                               <Card.Text>
                               Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                               Cum sociis natoque penatibus
                               </Card.Text>
                               <Card.Link href="#">Card Link</Card.Link>
                               <Card.Link href="#">Another Link</Card.Link>
                   </Card.Body>
                  </Card>
                  <Card className="card-primary" style={{ width: '18rem', marginRight:"10px", marginBottom:"8px" }}>
                  <Card.Body>
                        <Card.Title>Blog</Card.Title>
                          <Card.Subtitle className="mb-2 text-muted">Blog</Card.Subtitle>
                              <Card.Text>
                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                              Cum sociis natoque penatibus
                              </Card.Text>
                              <Card.Link href="#">Card Link</Card.Link>
                              <Card.Link href="#">Another Link</Card.Link>
                  </Card.Body>
                 </Card>
                  </Row>
                  <Row xs={1} md={2} className="g-4" style={{marginLeft:"20px"}}>
                   <Card className="card-primary" style={{ width: '18rem', marginRight:"10px", marginBottom:"8px" }}>
                   <Card.Body>
                         <Card.Title>Blog</Card.Title>
                           <Card.Subtitle className="mb-2 text-muted">Blog</Card.Subtitle>
                               <Card.Text>
                               Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                               Cum sociis natoque penatibus
                               </Card.Text>
                               <Card.Link href="#">Card Link</Card.Link>
                               <Card.Link href="#">Another Link</Card.Link>
                   </Card.Body>
                  </Card>
                  <Card className="card-primary" style={{ width: '18rem', marginRight:"10px", marginBottom:"8px" }}>
                  <Card.Body>
                        <Card.Title>Blog</Card.Title>
                          <Card.Subtitle className="mb-2 text-muted">Blog</Card.Subtitle>
                              <Card.Text>
                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                              Cum sociis natoque penatibus
                              </Card.Text>
                              <Card.Link href="#">Card Link</Card.Link>
                              <Card.Link href="#">Another Link</Card.Link>
                  </Card.Body>
                 </Card>

                 <Card className="card-primary" style={{ width: '18rem', marginRight:"10px", marginBottom:"8px" }}>
                 <Card.Body>
                       <Card.Title>Blog</Card.Title>
                         <Card.Subtitle className="mb-2 text-muted">Blog</Card.Subtitle>
                             <Card.Text>
                             Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                             Cum sociis natoque penatibus
                             </Card.Text>
                             <Card.Link href="#">Card Link</Card.Link>
                             <Card.Link href="#">Another Link</Card.Link>
                 </Card.Body>
                </Card>
                <Card className="card-primary" style={{ width: '18rem', marginRight:"10px", marginBottom:"8px" }}>
                <Card.Body>
                      <Card.Title>Blog</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">Blog</Card.Subtitle>
                            <Card.Text>
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                            Cum sociis natoque penatibus
                            </Card.Text>
                            <Card.Link href="#">Card Link</Card.Link>
                            <Card.Link href="#">Another Link</Card.Link>
                </Card.Body>
               </Card>
                </Row>

                  </Content>
          </div>
          </Container>
        </main>


);

}
