import React, { useState, useEffect, useRef } from "react";
import { useParams,  Link as NavLink} from 'react-router-dom'
import {Link as ScollLink} from 'react-scroll'
import ScrollTopArrow from './scrollToTop'
import logo from './cafeTerrrainEdit.png'
import styled, { createGlobalStyle } from "styled-components";
import {Navbar, HamburgerButton, Styles, CSSReset} from './styles'

// TODO: make styled component for the dropdown
// mobile version of dropdown has a position right:20px;
// TODO: have the main nav page send the user to main page and then
// the dropdown will have options for the secondary pages
// TODO: think of a dropdown for mobile devices
// think of a way to deactivate hover if user is on the wrong page
// check to see if there are conditionals based on component

function NavBar() {
  const [openDrawer, toggleDrawer] = useState(false);
  const drawerRef = useRef(null);



  useEffect(() => {
    /* Close the drawer when the user clicks outside of it */
    const closeDrawer = event => {
      if (drawerRef.current && drawerRef.current.contains(event.target)) {
        return;
      }

      toggleDrawer(false);
    };

    document.addEventListener("mousedown", closeDrawer);
    return () => document.removeEventListener("mousedown", closeDrawer);
  }, []);

  var elementExists = document.getElementById("Home");



  return (
    <Styles.Wrapper>
      <CSSReset />

      <Navbar.Wrapper>
        <img src={logo} width={40} height={50} alt="cafe Terrrain Logo"/>
        <HamburgerButton.Wrapper onClick={() => toggleDrawer(true)}>
          <HamburgerButton.Lines />
        </HamburgerButton.Wrapper>

        <Navbar.Items ref={drawerRef} openDrawer={openDrawer}>
          <Navbar.Item>
          <div className="dropdown">
  <NavLink  to="/">Home</NavLink>
  {<div class="dropdown-content">
    <ScollLink  to="About" spy={true} smooth={true}>About Us</ScollLink>
    <ScollLink  to="team" spy={true} smooth={true}>Our team</ScollLink>
  </div>}
        </div>
          </Navbar.Item>
          <Navbar.Item>
                <div className="dropdown">
                  <a style={{textDecoration:"underline"}}>Services</a>
                  <div class="dropdown-content">
                    <NavLink to="packages">Packages</NavLink>
                    <NavLink to="classes">Classes</NavLink>
                    <NavLink  to="Services">Our Services</NavLink>
                    <NavLink to="quiz">Quiz</NavLink>
                  </div>
                </div>
          </Navbar.Item>
          <Navbar.Item>
              <NavLink  to="Blog">Blog</NavLink>
          </Navbar.Item>
          <Navbar.Item>
                    <NavLink  to="portfolio">Portfolio</NavLink>
          </Navbar.Item>
          <Navbar.Item>
              <NavLink  to="Contact">Contact</NavLink>
          </Navbar.Item>
        </Navbar.Items>
      </Navbar.Wrapper>
    </Styles.Wrapper>
  );
}



export default NavBar;
