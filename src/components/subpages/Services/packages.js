import React, {  useEffect } from 'react';
import {Container} from "react-bootstrap"

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
//import background from "./background.jpg"
//import image from "./backgroundEdit.jpg"

// TODO: Add section for 'Our mission'

export default function Packages(){


  useEffect(() => {
 document.title = "Packages"
}, []);



return (


  <main className="relative" style={{justifyContent:"center", height:"100vh", backgroundColor:'rgb(221, 215, 195)' ,zIndex:-1}}>
  <header>
  <title>Packages</title>
  </header>
  <Container>
          <div id="Packages" className="p-10 lg:pt-48 container mx-auto relative" style={{paddingTop:"50px",backgroundColor:"rgb(221, 215, 195)"}}>
              <section  style={{backgroundColor:'#BFC8D1'}} className="rounded-lg lg:flex p-20 my-10">

                  <div className="text-sm lg:text-lg flex flex-col justify-center">
                  <h1>Package 1</h1>
                  <h4>Web design and development</h4>
                    <div>
                          Open your virtual branch of your business with Cafeterrain's expert team.
                          We are helping you from branding, website designing & building and how to maintain it.
                    </div>
                  </div>
              </section>
              <section  style={{backgroundColor:'#BFC8D1'}} className="rounded-lg lg:flex p-20 my-10">

                  <div className="text-sm lg:text-lg flex flex-col justify-center">
                  <h1>Package 2</h1>
                  <h4>Social media and Set-up development</h4>
                    <div>
                    Make your business a food celebrity on social media with Cafeterrain.
                    Our experts can set up and manage your business' social media accounts.
                    We connect your business with influencers.
                    </div>
                  </div>
              </section>
              <section  style={{backgroundColor:'#BFC8D1'}} className="rounded-lg lg:flex p-20 my-10">

                  <div className="text-sm lg:text-lg flex flex-col justify-center">
                  <h1>Package 3</h1>
                  <h4>Online Advertising Set-up and optimization </h4>
                    <div>

Specialized in Google My Business, Yelp, Uber Eats
Branding Strategy
Original content design

                    </div>
                  </div>
              </section>
          </div>
          </Container>
        </main>


);

}
