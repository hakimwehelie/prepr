import React, {  useEffect } from 'react';
import { Container} from "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
//import background from "./background.jpg"
// import image from "./backgroundEdit.jpg"

export default function AppPage(){


  useEffect(() => {
 document.title = "App"
}, []);



return (


  <main className="relative" style={{justifyContent:"center", height:"100vh", backgroundColor:'#BFFFD1' ,zIndex:-1}}>
  <header>
  <title>Our App</title>
  </header>
  <Container>
          <div id="Portfolio" className="p-10 lg:pt-48 container mx-auto relative" style={{backgroundColor:"#BFFFD1"}}>
              <section className="bg-green-600 rounded-lg shadow-2xl lg:flex p-20 my-10">

                  <div className="text-sm lg:text-lg flex flex-col justify-center text-yellow-400 cursive">
                  <h1>Our App</h1>
                    <div>
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                          Cum sociis natoque penatibus et magnis dis parturient montes,
                          nascetur ridiculus mus. Donec quam felis, ultricies nec,
                          pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                          Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                          In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                    </div>
                  </div>
              </section>
          </div>
          </Container>
        </main>


);

}
