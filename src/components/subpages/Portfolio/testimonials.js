import React, { useEffect } from 'react';
import {Container} from "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Content} from '../../styles'
import styled from 'styled-components'


export default function Testimonials(){


  useEffect(() => {
 document.title = "Portfolio"
}, []);




return (


  <main className="relative" style={{justifyContent:"center", height:"100vh", backgroundColor:'#ebeae5' ,zIndex:-1}}>
  <Container>
  <div>
      <Content id="cases" className="p-10 lg:pt-48 container mx-auto relative" style={{backgroundColor:"#ebeae5" }}>
          <section className=" rounded-lg  lg:flex p-20 my-10">

              <div className="text-sm lg:text-lg flex flex-col justify-center">
              <h1>Testimonials</h1>
                <div>
                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                      Cum sociis natoque penatibus et magnis dis parturient montes,
                      nascetur ridiculus mus. Donec quam felis, ultricies nec,
                      pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                      Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                      In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                </div>

              </div>
          </section>

      </Content>
      </div>
          </Container>
        </main>


);

}
