import React, {  useEffect } from 'react';
import { Container} from "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
//import background from "./background.jpg"
//import image from "./backgroundEdit.jpg"
import alex from "../../alexander.jpg"
import logo from '../../earlyBirdLogo.png'
import team from '../../media/team.png'
import {Content} from '../../styles'
// TODO: Add section for 'Our mission'

export default function About(){


  useEffect(() => {
 document.title = "About page"
}, []);



return (


  <main className="relative" style={{justifyContent:"center", height:"100%", backgroundColor:'#ddd7c3' ,zIndex:-1}}>
  <header>
  <title>Home page</title>
  </header>
  <Container>
          <div id="About" className="p-10 lg:pt-48 container mx-auto relative" style={{backgroundColor:"#ddd7c3", textAlign:"center"}}>
                                  <h2 style={{fontStyle:"oblique"}}>About Us</h2>
                                  <div>
                                  We are a diverse team of experts aiming to support local bussinesses in the food indusrty
                                  <br/>
                                  Seeing how much cafes and restruants struggle during the pandememic, we feel the urge to help them
                                  <br/>
                                  We created a platform for them and their customers to connect and communicate with each other better
                                  <br/>
                                  We are here to help your bussiness improve: digital marketing, such as building a website,
                                  <br/>
                                  creating mobile friendly acessable menus, and increasing your online presence for more awareness
                                  </div>
                                  </div>
                          <div style={{display:"flex", justifyContent:"center"}}>
              <img src={alex} width="70%" height="60%"/>
                          </div>
          <div style={{textAlign:"center", display:"flex", marginTop:"20px"}}>
                  <div className="text-sm lg:text-lg flex flex-col justify-center">
                  <h2 style={{fontStyle:"oblique"}}>Our mission</h2>
                    <div>
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                          Cum sociis natoque penatibus et magnis dis parturient montes,
                          nascetur ridiculus mus. Donec quam felis, ultricies nec,
                          pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                          Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                          In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                    </div>
                  </div>

          </div>
          <Content  className="p-10 lg:pt-48 container mx-auto relative" style={{paddingTop:"0px" }}>
      <section className=" rounded-lg  lg:flex p-20 my-10">
        <img src={team}/>
      </section>

  </Content>
          </Container>
        </main>


);

}
