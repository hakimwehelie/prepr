import React, {  useEffect } from 'react';
import { Container} from "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import image from'../../backgroundEdit.jpg'

//TODO: Clean up the about us section

export default function HomePage(){

  useEffect(() => {
 document.title = "Home page"

}, []);

  return (


    <main className="relative" style={{display:"grid",justifyContent:"center", height:"100vh", backgroundSize: "cover" ,zIndex:-1
  ,backgroundImage:`url(${image})`, width:"100vw"}}>
    <header>
    <title>Home page</title>
    </header>
    <Container>
    <div id="cases" className="p-10 lg:pt-48 container mx-auto relative" style={{textAlign:"center" }}>
      <section className=" rounded-lg  lg:flex p-20 my-10">



                    <div style={{display:"grid", position:"relative", bottom:"155px"}} className="text-sm lg:text-lg flex flex-col justify-center">
                    <h1 style={{fontStyle:"oblique"}}>Hello and welcome to our home page.</h1>
                    <h2> We help restruant and cafe owners start their journey into digital marketing for their bussiness.</h2>

                    </div>
                </section>
            </div>
            </Container>

          </main>


  );

}
