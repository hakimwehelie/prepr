import React, {  useEffect } from 'react';
import HomePage from './subpages/HomePage/homePg'
import About from './subpages/HomePage/about'
// import {Card, Button, Container} from "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
//import background from "./background.jpg"
// import image from "./backgroundEdit.jpg"

export default function Home(){

  useEffect(() => {
 document.title = "Home page"
}, []);

  return (

          <div>
          <HomePage/>
          <About/>
          </div>

  );

}
