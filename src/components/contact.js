import React, {  useEffect } from 'react';
import {Container} from "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
//import background from "./background.jpg"
//import image from "./backgroundEdit.jpg"

// TODO: Add section for 'Our mission'

export default function Contact(){


  useEffect(() => {
 document.title = "Contact"
}, []);



return (


  <main className="relative" style={{textAlign:'center',justifyContent:"center", height:"100vh", backgroundColor:'#ebeae5' ,zIndex:-1}}>
  <header>
  <title>Home page</title>
  </header>
  <Container>
          <div style={{paddingTop:'60px'}}>
          <h1 style={{fontStyle:"oblique"}}>Contact us at:</h1>
                <div>
                Phone:XXX-XXX-XXXX
                <br/>
                Fax:XXX-XXX-XXXX
                <br/>
                Adress: 2131 JO Ave, Ontario CA
                </div>
          </div>
          <div id="Contact" className="p-10 lg:pt-48 container mx-auto relative" style={{backgroundColor:"#ebeae5"}}>
              <section style={{backgroundColor:'#3C462D'}}  className=" rounded-lg   p-20 my-10">
            <form id='contact-form'  style={{backgroundColor:'#3C462D'}} class="rounded px-8 my-8 pt-6 pb-8 mb-4">
        <label style={{color:"#d1dbc9"}}>Name</label>
        <input  name='user_name' type='text'  placeholder='Name' class="shadow my-3 appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" required/>
        <br/>
        <label style={{color:"#d1dbc9"}}>Email address</label>
        <input  name='user_email' type='email'  placeholder='Email' class="shadow my-3 appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"required/>
        <br/>
        <label style={{color:"#d1dbc9"}}>Tell us how we can help</label>
        <textarea  name='message' placeholder='Message'class="shadow my-3 appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"required/>
        <br/>
        <input type='submit' value='Send' class="bg-white-400 hover:bg-yellow-400 text-black-300 hover:text-blue-500 font-bold py-2 px-4 rounded w-auto"/>
      </form>
              </section>
          </div>
          </Container>
        </main>


);

}
