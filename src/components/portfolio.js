import React, {  useEffect } from 'react';
// import {Card, Button, Container} from "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Cases from './subpages/Portfolio/cases'
import Testimonials from './subpages/Portfolio/testimonials'
import QuizLink from './quizLink'
import {Container} from 'react-bootstrap'
import page from './media/portfolioCrop.png'

export default function Portfolio(){

// Wireframe image is a placeholder

  useEffect(() => {
 document.title = "Portfolio"
}, []);



return (

  <main className="relative" style={{justifyContent:"center", height:"100%", backgroundColor:'rgb(221, 215, 195)' ,zIndex:-1}}>

  <Container>

  <div id="quiz" className="p-10 lg:pt-48 container mx-auto relative" style={{backgroundColor:"rgb(221, 215, 195)" }}>
      <section className=" rounded-lg  lg:flex p-20 my-10">

                  <div   className="text-sm lg:text-lg flex flex-col justify-center">
                    <div>
                          <img src={page} style={{maxHeight:"100%",maxWidth:"100%"}}/>
                    </div>
                  </div>
              </section>
          </div>
          </Container>
        </main>


);

}
/*
<main className="relative" style={{justifyContent:"center", height:"100vh", backgroundColor:'#BFC8D1' ,zIndex:-1}}>
<header>
<title>Home page</title>
</header>
<Container>
        <div id="Portfolio" className="p-10 lg:pt-48 container mx-auto relative" style={{backgroundColor:"#BFC8D1"}}>
            <section className="bg-green-600 rounded-lg shadow-2xl lg:flex p-20 my-10">

                <div className="text-sm lg:text-lg flex flex-col justify-center text-yellow-400 cursive">
                <h1>Our Portfolio</h1>
                  <div>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                        Cum sociis natoque penatibus et magnis dis parturient montes,
                        nascetur ridiculus mus. Donec quam felis, ultricies nec,
                        pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                        Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                        In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                  </div>
                </div>
            </section>
        </div>
        </Container>
      </main>
*/
