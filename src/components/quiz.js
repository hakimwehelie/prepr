import React, { useEffect } from 'react';
import {Container} from "react-bootstrap"
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import quiz from './questionsEdit.png'

// TQuiz Wireframe is a placeholder

export default function Quiz(){


  useEffect(() => {
 document.title = "Quiz"
}, []);



return (


  <main className="relative" style={{justifyContent:"center", height:"100%", backgroundColor:'rgb(221, 215, 195)' ,zIndex:-1}}>
  <header>
  <title>Quiz</title>
  </header>
  <Container>

  <div id="quiz" className="p-10 lg:pt-48 container mx-auto relative" style={{backgroundColor:"rgb(221, 215, 195)" }}>
      <section className=" rounded-lg  lg:flex p-20 my-10">

                  <div   className="text-sm lg:text-lg flex flex-col justify-center">
                  <h1>Quiz</h1>
                    <div>
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
                          Cum sociis natoque penatibus et magnis dis parturient montes,
                          nascetur ridiculus mus. Donec quam felis, ultricies nec,
                          pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                          Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                          In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                          <img src={quiz} style={{maxHeight:"100%",maxWidth:"100%"}}/>
                    </div>
                  </div>
              </section>
          </div>
          </Container>
        </main>


);

}
