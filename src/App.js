import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Bottom from './components/bottom'
// import About from './components/about'
import Home from './components/home'
import NavBar from './components/navbar'
import ScrollTopArrow from './components/scrollToTop'
import Services from './components/services'
import Portfolio from './components/portfolio'
import Blog from './components/blog'
import Contact from './components/contact'
import Quiz from './components/quiz'
import AppPage from './components/subpages/Services/AppPage'
import Packages from './components/subpages/Services/packages'
import Classes from './components/subpages/Services/classes'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

//// TODO: find a easy way to check screen size

function App() {

  return (

<BrowserRouter>
<NavBar/>

<Routes>
    <Route path='/' element={<Home />}/>
    <Route path='/services' element={<Services />}/>
    <Route path='/blog' element={<Blog/>}/>
    <Route path='/portfolio' element={<Portfolio />}/>
    <Route path='/contact' element={<Contact/>}/>
    <Route path='/AppPage' element={<AppPage/>}/>
    <Route path='/classes' element={<Classes/>}/>
    <Route path='/packages' element={<Packages/>}/>
    <Route path='/quiz' element={<Quiz/>}/>
</Routes>
<ScrollTopArrow/>
<Bottom/>
</BrowserRouter>


  );
}

export default App;
// TODO: prepare router to link the pages together

// function App() {
//   return (
//         <BrowserRouter>
//           <NavBar/>
//             <Switch>
//                 <Route component={Home} path='/' exact/>
//                 <Route component={About} path='/About'/>
//                 <Route component={Services} path='/Services'/>
//                 <Route component={Contact} path='/Contact'/>
//             </Switch>
//             <Bottom/>
//         </BrowserRouter>
//   );
// }
//
// export default App;


// return (
//
// <BrowserRouter>
//
// <NavBar/>
// <Routes>
// <Route path='/' element={<Home />}/>
// <Route path='/components/about' element={<About/>}/>
// <Route path='/components/services' element={<Services />}/>
// </Routes>
// <Bottom/>
// </BrowserRouter>
//
//
// );
